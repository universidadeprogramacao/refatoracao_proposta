<?php

/**
 * Description of Validation
 *
 * @author EricLuizFerrás
 */
namespace Util;
final class Validation {

    public static function isPhoneValid($phone) {
        if (!preg_match('/^[0-9]{11}$/', $phone)) {
            return false;
        }
        return true;
    }

}
