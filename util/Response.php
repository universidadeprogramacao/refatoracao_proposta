<?php

/**
 * Description of Response
 *
 * @author EricLuizFerrás
 */

namespace Util;

final class Response {

    public static function getResponseJson($title, $status, $code, $description, $subDescription) {

        $responseToEncode = array(
            $title => array(
                'Status' => $status,
                'Code' => $code,
                'Description' => $description,
                'Sub Description' => $subDescription
            )
        );
        return json_encode($responseToEncode);
    }

}
