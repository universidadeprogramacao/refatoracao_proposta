<?php

require_once './util/Response.php';
require_once './util/Validation.php';
require_once './model/BaseModel.php';
require_once './model/Usuario.php';
require_once './dao/BaseDAO.php';
require_once './dao/UsuarioDAO.php';

use Util\Response;
use Model\Usuario;
use DAO\UsuarioDAO;

/**
 * usuario['name']
 * usuario['user_name']
 * usuario['email']
 * usuario['password']
 */
if (!empty($_POST['usuario'])) {
    $data = $_POST['usuario'];
    $usuario = new Usuario();
    $usuario = $usuario->toObjectByArray($data);
    $validation = $usuario->isEntityValid($usuario);
    if (!$validation['type']) {
        echo Response::getResponseJson('Achados e Perdidos', 'error', '400', 'Bad Request', $validation['message']);
    }

    $usuarioDao = new UsuarioDAO();
    $usuarioDao->save($usuario);
}
