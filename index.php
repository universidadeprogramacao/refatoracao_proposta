<?php
    require_once '../Modules/Routes.php';
    require_once '../Modules/Definitions.php';
 
    Routes::POST(function(){
        $name = isset($_POST['name']) ? trim($_POST['name']) : null;
        $surname = isset($_POST['surname']) ? trim($_POST['surname']) : null;
        $phone = isset($_POST['phone']) ? trim($_POST['phone']) : null;
        $email = isset($_POST['email']) ? trim($_POST['email']) : null;
        $password = isset($_POST['password']) ? trim($_POST['password']) : null;
       
        if($name == null){
            $responseToEncode = array('Achados e Perdidos' => array('Status' => 'Error', 'Code' => '400', 'Description' => 'Bad Request', 'Sub Description' => 'Name is null or was not setted'));
            $responseToSend = json_encode($responseToEncode);
            die($responseToSend);
        }
        if($surname == null){
            $responseToEncode = array('Achados e Perdidos' => array('Status' => 'Error', 'Code' => '400', 'Description' => 'Bad Request', 'Sub Description' => 'Surname is null or was not setted'));
            $responseToSend = json_encode($responseToEncode);
            die($responseToSend);
        }
        if($phone == null){
            $responseToEncode = array('Achados e Perdidos' => array('Status' => 'Error', 'Code' => '400', 'Description' => 'Bad Request', 'Sub Description' => 'Phone is null or was not setted'));
            $responseToSend = json_encode($responseToEncode);
            die($responseToSend);
        }
        if($email == null){
            $responseToEncode = array('Achados e Perdidos' => array('Status' => 'Error', 'Code' => '400', 'Description' => 'Bad Request', 'Sub Description' => 'Email is null or was not setted'));
            $responseToSend = json_encode($responseToEncode);
            die($responseToSend);
        }
        if($password == null){
            $responseToEncode = array('Achados e Perdidos' => array('Status' => 'Error', 'Code' => '400', 'Description' => 'Bad Request', 'Sub Description' => 'Password is null or was not setted'));
            $responseToSend = json_encode($responseToEncode);
            die($responseToSend);
        }
       
        if(!preg_match('/^[A-Za-záéíóúàèìòùÁÉÍÓÚÀÈÌÒÙâêîôûÂÊÎÔÛãõñÃÕÑ]{1,}$/', $name)){
            $responseToEncode = array('Achados e Perdidos' => array('Status' => 'Error', 'Code' => '400', 'Description' => 'Bad Request', 'Sub Description' => 'Name is wrong'));
            $responseToSend = json_encode($responseToEncode);
            die($responseToSend);
        }
        if(!preg_match('/^[A-Za-záéíóúàèìòùÁÉÍÓÚÀÈÌÒÙâêîôûÂÊÎÔÛãõñÃÕÑ]{1,}$/', $surname)){
            $responseToEncode = array('Achados e Perdidos' => array('Status' => 'Error', 'Code' => '400', 'Description' => 'Bad Request', 'Sub Description' => 'Surname is wrong'));
            $responseToSend = json_encode($responseToEncode);
            die($responseToSend);
        }
        if(!preg_match('/^[0-9]{11}$/', $phone)){
            $responseToEncode = array('Achados e Perdidos' => array('Status' => 'Error', 'Code' => '400', 'Description' => 'Bad Request', 'Sub Description' => 'Phone is wrong'));
            $responseToSend = json_encode($responseToEncode);
            die($responseToSend);
        }
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
            $responseToEncode = array('Achados e Perdidos' => array('Status' => 'Error', 'Code' => '400', 'Description' => 'Bad Request', 'Sub Description' => 'Email is wrong'));
            $responseToSend = json_encode($responseToEncode);
            die($responseToSend);
        }
        if(!preg_match('/^[A-Za-z0-9@]{1,}$/', $password)){
            $responseToEncode = array('Achados e Perdidos' => array('Status' => 'Error', 'Code' => '400', 'Description' => 'Bad Request', 'Sub Description' => 'Password is wrong'));
            $responseToSend = json_encode($responseToEncode);
            die($responseToSend);
        }
       
        $connection = mysqli_connect(DATABASE_ADDRESS, DATABASE_USERNAME, DATABASE_PASSWORD, DATABASE_NAME) or die(function(){
            $responseToEncode = array('Achados e Perdidos' => array('Status' => 'Error', 'Code' => '500', 'Description' => 'Internal Server Error', 'Sub Description' => 'Error connecting to database'));
            $responseToSend = json_encode($responseToEncode);
            return $responseToSend;
        });
        $selection = mysqli_select_db($connection, DATABASE_NAME) or die(function(){
            $responseToEncode = array('Achados e Perdidos' => array('Status' => 'Error', 'Code' => '500', 'Description' => 'Internal Server Error', 'Sub Description' => 'Error selecting to database'));
            $responseToSend = json_encode($responseToEncode);
            return $responseToSend;
        });
       
        $query_0 = mysqli_query($connection, "SELECT `phone`, `email` FROM `users` WHERE `phone` = '$phone' OR `email` = '$email'")or die(function(){
            $responseToEncode = array('Achados e Perdidos' => array('Status' => 'Error', 'Code' => '500', 'Description' => 'Internal Server Error', 'Sub Description' => 'Error querying in database'));
            $responseToSend = json_encode($responseToEncode);
            return $responseToSend;
        });
        $query_0_fetched = mysqli_fetch_array($query_0) or die(function(){
            $responseToEncode = array('Achados e Perdidos' => array('Status' => 'Error', 'Code' => '500', 'Description' => 'Internal Server Error', 'Sub Description' => 'Error fetching response'));
            $responseToSend = json_encode($responseToEncode);
            return $responseToSend;
        });
        if($query_0 -> num_rows > 0){
            if($query_0_fetched['email'] == $email){
                $responseToEncode = array('Achados e Perdidos' => array('Status' => 'Error', 'Code' => 'xxx', 'Description' => 'xxx xxx xxx', 'Sub Description' => 'This email is not available'));
                $responseToSend = json_encode($responseToEncode);
                die($responseToSend);
            }
            if($query_0_fetched['phone'] == $phone){
                $responseToEncode = array('Achados e Perdidos' => array('Status' => 'Error', 'Code' => 'xxx', 'Description' => 'xxx xxx xxx', 'Sub Description' => 'This phone is not available'));
                $responseToSend = json_encode($responseToEncode);
                die($responseToSend);
            }
        }
       
        //CONTINUA
        //CONTINUA
        //CONTINUA
    });
 
    Routes::GET(function(){
        echo 'GET';
    });
 
    Routes::PUT(function(){
        echo 'PUT';
    });
 
    Routes::PATCH(function(){
        echo 'PATCH';
    });
 
    Routes::DELETE(function(){
        echo 'DELETE';
    });
 
    Routes::ANY(function(){
       echo 'ANY';
    });
?>