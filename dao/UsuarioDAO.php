<?php

namespace DAO;

use Model\Usuario;

/**
 * Description of UsuarioDAO
 *
 * @author EricLuizFerrás
 */
class UsuarioDAO extends BaseDAO {

    public function save(Usuario $object) {
        return parent::save($object);
    }

}
