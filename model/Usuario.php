<?php

namespace Model;

use Util\Validation;

/**
 * Description of Usuario
 *
 * @author EricLuizFerrás
 */
class Usuario extends BaseModel {

    private $name;
    private $userName;
    private $phone;
    private $email;
    private $password;

    public function setName($name) {
        $this->name = $name;
    }

    public function setUsername($userName) {
        $this->userName = $userName;
    }

    public function setPhone($phone) {
        $this->phone = $phone;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function setPassWord($password) {
        $this->password = $password;
    }

    public function getName() {
        return $this->name;
    }

    public function getUsername() {
        return $this->userName;
    }

    public function getPhone() {
        return $this->phone;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getPassWord() {
        return $this->password;
    }

    //centraliza todas validações da entidade
    public function isEntityValid(Usuario $objeto) {
        $data = array();
        $data['type'] = true;
        $data['message'] = '';
        if (!Validation::isPhoneValid($objeto->getPhone())) {
            $data['type'] = false;
            $data['message'] = 'Phone is wrong';
            return $data;
        }
        //restante das validações
        return $data;
    }

    /**
     * recebe o objeto e transforma em um array
     * pode ser util para popular os inputs na tela dinamicamente
     */
    public function toArrayByObject(Usuario $object) {
        $arr = array();
        $arr['name'] = $object->getName();
        $arr['user_name'] = $object->getUsername();
        $arr['phone'] = $object->getPhone();
        $arr['email'] = $object->getPhone();
        $arr['password'] = $object->getPassWord();
        return $arr;
    }

    /**
     * recebe os inputs do banco de dados ou do form e transforma em objeto
     */
    public function toObjectByArray(array $arr) {
        $usuario = new Usuario();
        $usuario->setName($arr['name']);
        $usuario->setUsername($arr['user_name']);
        $usuario->setEmail($arr['email']);
        $usuario->setPassWord($arr['password']);
        return $usuario;
    }

}
