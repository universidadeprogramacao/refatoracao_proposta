<?php

namespace Model;

/**
 * Description of BaseModel
 *
 * @author EricLuizFerrás
 */
abstract class BaseModel {

    public abstract function isEntityValid(BaseModel $objeto);

    public abstract function toObjectByArray(array $arr);

    public abstract function toArrayByObject(BaseModel $object);
}
